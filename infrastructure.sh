#!/bin/sh
### docker ###
yum update -y
amazon-linux-extras install docker -y
service docker start
systemctl enable docker

### docker-compose ###
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

### vim htop git ###
yum install vim htop git -y

### gitlab runner ###
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
sudo yum install gitlab-runner -y

### add ec2-user and gitlab-runner to docker group ###
sudo usermod -aG docker ec2-user gitlab-runner

### git clone ###
git clone https://gitlab.com/Georg-get/coffe.git

### docker-compose start ###
cd  coffe-pet-project ; docker-compose up -d
