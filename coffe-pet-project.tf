provider "aws" {
}


resource "aws_instance" "coffe-pet-project" {
  #1. Choose AMI
  ami                       = "ami-0a334376f1b255321" # Amazon Linux 2 AMI

  #2. Choose Instance Type
  instance_type             = "t3.micro"

  #3. Configure Instance

  user_data                 = file("infrastructure.sh") # <<EOF код скрипта #EOF
 
  #4. Add Storage
  ebs_block_device {
    device_name           = "/dev/xvda"
    #encrypted             = false
    #iops                  = 100/3000 
    volume_size           = 10
    volume_type           = "gp2"
    delete_on_termination = true
  }

  #5. Add Tags
  tags                      = {
    "Name"                  = "coffe-pet-project"
  }

  #6. Configure Security Group
  vpc_security_group_ids    = ["sg-03043cdc0c38921d1"] # ["Web+Ssh+Zabbix-agent"]
  
  #7. Review
  #key_name                  = "key-Milan.pem" 

}

resource "aws_eip" "coffe-pet-project" {
    instance                = aws_instance.coffe-pet-project.id
    public_ipv4_pool        = "amazon"
    vpc                     = true

  tags                      = {
    Name                    = "coffe-pet-project"
  }
}

output "EIP" {
  value                     = aws_eip.coffe-pet-project.public_ip
}

#resource "aws_security_group" "Web+Ssh+Zabbix-agent" {
  #name        = "WebServer"
  #description = "Full web server setup"

  #ingress {
    #description = "Allow port SSH"
    #from_port   = 22
    #to_port     = 22
    #protocol    = "tcp"
    #cidr_blocks = ["0.0.0.0/0"]
  #}

  #ingress {
    #description = "Allow port HTTP"
    #from_port   = 80
    #to_port     = 80
    #protocol    = "tcp"
    #cidr_blocks = ["0.0.0.0/0"]
  #}

  #ingress {
    #description = "Allow port HTTPS"
    #from_port   = 443
    #to_port     = 443
    #protocol    = "tcp"
    #cidr_blocks = ["0.0.0.0/0"]
  #}

  #ingress {
    #description = "Allow Zabbix agent port"
    #from_port   = 10051
    #to_port     = 10051
    #protocol    = "tcp"
    #cidr_blocks = ["0.0.0.0/0"]
  #}

  #egress {
    #description = "Allow ALL ports"
    #from_port   = 0
    #to_port     = 0
    #protocol    = "-1"
    #cidr_blocks = ["0.0.0.0/0"]
  #}
